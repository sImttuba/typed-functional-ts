class Logger {
  log(msg: string): void {
    console.log(msg);
  }
}
export module Main {
  interface SearchFunc {
    (source: string, subString: string): boolean
  }

  let mSearch: SearchFunc =
    (s1: string, s2: string) => s1.search(s2) != -1;

  const logger = new Logger();
  logger.log("hello world!");
}