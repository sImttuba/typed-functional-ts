export module SmokeTest
{
  export function HelloWorld(txt:string):string {
    return txt ? `Hello World ${txt}`: "Hello World"
  }

}