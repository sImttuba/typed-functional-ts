import {property, string} from "fast-check";
import {assert} from "fast-check";
// @ts-ignore
import {SmokeTest} from "../out/";
import {Main} from "../out/Main";

const contains = (text: string, pattern: string)
  : boolean => text.indexOf(pattern) >= 0;

describe("string properties", () => {
  it("string should always contain itself'", () => {
    assert(property(string(),
        text => contains(SmokeTest.HelloWorld(text), SmokeTest.HelloWorld(text))))
  })
});

describe("mSearch equals last", () => {
  it("test", () => {
    assert(property(string(),
      text => true))
  })
});